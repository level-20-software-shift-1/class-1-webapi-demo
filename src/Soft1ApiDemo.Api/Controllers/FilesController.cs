using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Soft1ApiDemo.Infrastructure.Utils;
using Soft1ApiDemo.Infrastructure.Cto;
using Soft1ApiDemo.Infrastructure.File;

namespace Soft1ApiDemo.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FilesController : ControllerBase
    {
        private readonly IFileUpload _fileLoad;

        public FilesController(IFileUpload fileLoad)
        {
            _fileLoad = fileLoad;
        }

        [HttpPost]
        [Route("/files")]
        public async Task<string> OnPostUploadAsync(IFormCollection files)
        {
            var list = await _fileLoad.UploadFiles(files);

            if (list.Count() > 0)
            {
                var res = new JsonResultDto
                {
                    Code = 1000,
                    Msg = "上传成功",
                    Data = list
                };

                return res.JsonSerialize();
            }
            else
            {
                var res = new JsonResultDto
                {
                    Code = 4000,
                    Msg = "上传失败，有不允许上传的文件扩展名或超出允许的文件大小",
                    Data = list
                };

                return res.JsonSerialize();
            }
        }

        [HttpGet]
        [Route("/files/{id}")]
        public async Task<FileContentResult> Get(Guid id)
        {
            var fileInfo=await _fileLoad.GetFile(id);
            return fileInfo;
        }
    }
}