using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Soft1ApiDemo.Domain.Entity;
using Soft1ApiDemo.Domain.Repository;
using Soft1ApiDemo.Infrastructure.Filters;
using Soft1ApiDemo.Infrastructure.Parameter;
using Soft1ApiDemo.Infrastructure.RequestParameter;
using Soft1ApiDemo.Infrastructure.Utils;

namespace Soft1ApiDemo.Api
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IConfiguration _configuration;

        private readonly IRepository<Users> _userRes;

        public UsersController(
            IRepository<Users> userRes,
            ILogger<UsersController> logger,
            IConfiguration configuration
        )
        {
            _userRes = userRes;
            _logger = logger;
            _configuration = configuration;
        }

        public IEnumerable<dynamic> Index()
        {
            // throw new Exception("你好老乡，这里有一个世纪错误，等待你的解决");
            var list =
                _userRes
                    .Table
                    .Select(x => new { x.Id, x.Username, x.Password })
                    .ToList();
            var lst =
                new List<dynamic> {
                    new { Avatar = "888", Money = 250 },
                    new { Avatar = "中国人民银行", Money = 250 },
                    new { Product = "5555", Money = 250 },
                    new { Avatar = "333", Height = 250 }
                };
            _logger.LogInformation(lst.JsonSerialize());
            return lst;
        }

        [Route("{id}/friends")]
        public IEnumerable<dynamic> Index(Guid id)
        {
            var list =
                _userRes
                    .Table
                    .Select(x => new { x.Id, x.Username, x.Password })
                    .ToList();
            var lst =
                new List<dynamic> {
                    new { Nickname = "吴江雄", Fat = 33 },
                    new { Nickname = "莫小贝", Fat = 33 },
                    new { Nickname = "排山倒海", Fat = 33 },
                    new { Nickname = "悠悠", Fat = 33 }
                };
            return lst;
        }

        [Route("{id}/friends/{fid}")]
        public dynamic Index(Guid id, Guid fid)
        {
            var list =
                _userRes
                    .Table
                    .Select(x => new { x.Id, x.Username, x.Password })
                    .ToList();
            var lst =
                new
                {
                    Code = 1000,
                    Msg = "获取指定用户下的指定朋友成功",
                    Data = new { id, fid }
                };
            return lst;
        }

        [HttpPost]
        public Users CreateUser(UserCto model)
        {
            System.Console.WriteLine(model);
            var user =
                new Users
                {
                    Username = model.Username,
                    Password = model.Password
                };
            var list = _userRes.Add(user);
            return list;
        }

        [HttpPut]
        [Route("{id}")]
        public Users UpdateUsers(Guid id, UserCto model)
        {
            var user = _userRes.GetById(id);
            if (user != null)
            {
                user.IsDeleted = model.IsDeleted;
                _userRes.Update(user);
            }

            return user;
        }

        [HttpDelete]
        [Route("{id}")]
        public dynamic DeleteUser(Guid id)
        {
            _userRes.Delete(id);
            return new { Code = 1000, Msg = "删除成功", Data = "" };
        }


        [AllowAnonymous]
        [Route("/token")]
        public string Login()
        {
            var username = "admin";
            var rolename = "超级管理";
            // var password="113";

            var tokenParameter = _configuration.GetSection("TokenParameter").Get<TokenParameter>();

            var token = TokenHelper.GenerateToken(tokenParameter, username, rolename);
            var res = new
            {
                Code = 1000,
                Msg = "登录成功",
                Data = token
            };
            return res.JsonSerialize();
        }

        [AllowAnonymous]
        [Route("/refreshtoken")]
        public string Login(AccessTokenParameter accessTokenParameter)
        {
            var tokenParameter = _configuration.GetSection("TokenParameter").Get<TokenParameter>();
            var tokenCto = TokenHelper.RefreshToken(accessTokenParameter.AccessToken, tokenParameter);
            return new
            {
                Code = 1000,
                Msg = "刷新AccessToken成功",
                Data = tokenCto.AccessToken
            }.JsonSerialize();
        }
    }

    public class UserCto
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public bool IsActived { get; set; }

        public bool IsDeleted { get; set; }
    }
}
