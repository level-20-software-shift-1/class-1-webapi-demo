using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace Soft1ApiDemo.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var environment =
                Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var config =
                new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json",
                    optional: true,
                    reloadOnChange: true)
                    .AddJsonFile($"appsettings.{environment}.json",
                    optional: true,
                    reloadOnChange: true)
                    .Build();

            Log.Logger =
                new LoggerConfiguration()
                    .ReadFrom
                    .Configuration(config)
                    .CreateLogger();

            // Log.Logger =
            //     new LoggerConfiguration()
            //         .MinimumLevel
            //         .Override("Microsoft", LogEventLevel.Information)
            //         .Enrich
            //         .FromLogContext()
            //         .WriteTo
            //         .Console()
            //         .CreateLogger();
            try
            {
                Log.Information("项目启动");
                CreateHostBuilder(args).Build().Run();
                // return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                // return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
            // CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host
                .CreateDefaultBuilder(args)
                .UseSerilog() //
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
