using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Soft1ApiDemo.Domain.Repository;
using Soft1ApiDemo.Infrastructure.Repository;
using Soft1ApiDemo.Infrastructure.Db;
using Microsoft.EntityFrameworkCore;
using Soft1ApiDemo.Infrastructure.Filters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Soft1ApiDemo.Infrastructure.Parameter;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Soft1ApiDemo.Infrastructure.File;

namespace Soft1ApiDemo.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var _conString = Configuration.GetConnectionString("SqlServer");
            services.AddDbContext<Soft1ApiDemoDbContext>(options => options.UseSqlServer(_conString));

            // 依赖注入 将CRUD的实现类注册到接口
            services.AddScoped(typeof(IRepository<>), typeof(RepositoryBase<>));
            services.AddScoped(typeof(IFileUpload), typeof(FileUpload));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(option =>
                {
                    option.RequireHttpsMetadata = false;//配置是否为https协议
                    option.SaveToken = true;//配置token是否保存在api上下文

                    var tokenParameter = Configuration.GetSection("TokenParameter").Get<TokenParameter>();
                    option.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret)),
                        ValidIssuer = tokenParameter.Issuer,
                        ValidateIssuer = true,
                        ValidateAudience = false
                    };
                });

            // 第一种方式
            // var ser=services.BuildServiceProvider();
            // var iRes=ser.GetService<ILogger<CustomGlobalExceptionFilter>>();

            // services.AddScoped<CustomGlobalExceptionFilter>();

            // services.AddControllers(options=>options.Filters.Add(new CustomGlobalExceptionFilter(iRes)));
            // services.AddControllers();

            // 第二种方式
            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(CustomGlobalExceptionFilter));
                options.Filters.Add(typeof(AuditLogFilter));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            //注册认证中间件，必须在鉴权中间件前面
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
