using System;

namespace Soft1ApiDemo.Domain
{
    /// <summary>
    /// 基础实体类，包含一些审计属性
    /// </summary>
    public abstract class BaseEntity
    {
        // 主键
        public Guid Id { get; set; }

        // 标识记录是否启用
        public bool IsActived { get; set; }

        // 标识记录是否删除
        public bool IsDeleted { get; set; }

        // 创建者的Id
        public Guid? CreatedBy { get; set; }

        // 修改者的Id
        public Guid? UpdatedBy { get; set; }

        // 创建时间
        public DateTime CreatedAt { get; set; }

        // （最后）修改时间
        public DateTime UpdatedAt { get; set; }

        // 展示的顺序
        public int DisplayOrder { get; set; }

        // 备注
        public string Remarks { get; set; }
    }
}
