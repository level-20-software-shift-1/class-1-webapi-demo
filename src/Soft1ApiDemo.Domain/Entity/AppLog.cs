namespace Soft1ApiDemo.Domain.Entity
{
    public class AppLog:BaseEntity
    {
        public string Message { get; set; }
        public string Exception { get; set; }
    }
}
