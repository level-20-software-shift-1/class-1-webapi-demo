namespace Soft1ApiDemo.Domain.Entity
{
    public class UploadFileInfo:BaseEntity
    {
        public string OriginFileName { get; set; }
        public string CurrentFileName { get; set; }
        public string RelativePath { get; set; }
        public long FileSize { get; set; }
        public string FileType { get; set; }
    }
}
