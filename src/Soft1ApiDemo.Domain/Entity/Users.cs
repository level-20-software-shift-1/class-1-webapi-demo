namespace Soft1ApiDemo.Domain.Entity
{
    public class Users : BaseEntity
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string Nickname { get; set; }

        public string Avatar { get; set; }
    }
}
