using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soft1ApiDemo.Domain.Repository
{
    public interface IRepository<T>
    {
        T GetById(Guid id);
        Task<T> GetByIdAsync(Guid id);

        IQueryable<T> Table{get;}

        T Add(T entity);

        Task<IEnumerable<T>> AddBulk(IEnumerable<T> entities);

        void Update(T entity);

        void Delete(Guid id);
    }
}
