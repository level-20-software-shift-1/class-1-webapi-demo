using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soft1ApiDemo.Infrastructure.Cto
{
    public class AccessTokenCto
    {
        public string AccessToken{get;set;}
        public string RefreshToken{get;set;}
    }
}