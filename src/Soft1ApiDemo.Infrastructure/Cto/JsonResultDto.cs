using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soft1ApiDemo.Infrastructure.Cto
{
    public class JsonResultDto
    {
        public int Code { get; set; }
        public string Msg { get; set; }
        public object Data { get; set; }
    }
}