using Soft1ApiDemo.Domain.Entity;
using Microsoft.EntityFrameworkCore;

namespace Soft1ApiDemo.Infrastructure.Db
{
    public class Soft1ApiDemoDbContext : DbContext
    {
        private readonly string
            _conString = "server=.;database=Soft1ApiDemo;uid=sa;pwd=123456;";

        public Soft1ApiDemoDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer(_conString);
        }

        public DbSet<Users> Users { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<UserRoles> UserRoles { get; set; }
        public DbSet<AppLog> AppLogs { get; set; }
        public DbSet<AuditLog> AuditLog { get; set; }
        public DbSet<UploadFileInfo> UploadFileInfo { get; set; }
    }
}
