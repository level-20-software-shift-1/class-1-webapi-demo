using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Soft1ApiDemo.Infrastructure.File
{
    public interface IFileUpload
    {
        Task<IEnumerable<string>> UploadFiles(IFormCollection files);
        Task<FileContentResult> GetFile(Guid id);
    }
}