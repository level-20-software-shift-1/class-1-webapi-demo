using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soft1ApiDemo.Infrastructure.Parameter
{
    public class TokenParameter
    {
        public string Secret { get; set; }
        public string Issuer { get; set; }
        public double AccessExpiration { get; set; }
        public double RefreshExpiration { get; set; }
    }
}