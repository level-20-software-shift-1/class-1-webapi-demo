using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soft1ApiDemo.Infrastructure.RequestParameter
{
    public class AccessTokenParameter
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}