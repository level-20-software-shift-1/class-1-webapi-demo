using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace Soft1ApiDemo.Infrastructure.Utils
{
    public static class JsonHelper
    {
        public static string JsonSerialize(this object obj)
        {
            var options = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
                WriteIndented = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };


            return JsonSerializer.Serialize(obj, options);
        }
    }
}
